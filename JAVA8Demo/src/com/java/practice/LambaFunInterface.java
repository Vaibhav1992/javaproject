package com.java.practice;

@FunctionalInterface
interface interf1 {
	public void m1();
}

@FunctionalInterface
interface interf2 {
	public void m2(int a, int b);
}

@FunctionalInterface
interface interf3 {
	public int m3(String s);
}

public class LambaFunInterface {
	public static void main(String[] args) {

		interf1 i1 = () -> System.out.println("Hi vaibhav lambava");
		i1.m1();
		interf2 i2 = (a, b) -> System.out.println("Hi vaibhav lambava sum is and b is : " + (a + b));
		i2.m2(5, 5);

		interf3 i3 = (s) -> s.length();
		System.out.println("length of vaibhav is :" + i3.m3("vaibhav"));

	}

}

class MyRunnable implements Runnable {

	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Child Thread");
		}
	}
}

class ThreadDemo {

	public static void main(String[] args) {
		/*
		 * Runnable r = new MyRunnable(); Thread t = new Thread(r); t.start(); // normal
		 * way for(int i=0; i<10; i++) { System.out.println("Main Thread"); }
		 */

		Runnable r2 = () -> {
			for (int i = 0; i < 10; i++) {
				System.out.println("child Thread in functional interface"); // Functional interface
			}

		};
		Thread t2 = new Thread(r2);
		t2.start();
		for (int i = 0; i < 10; i++) {
			System.out.println("Main Thread");
		}
	}
}

class Test {

	public static void main(String[] args) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				for (int i = 0; i < 10; i++) {
					System.out.println("Child Thread");
				}
			}
		}); // inner class
		t.start();
		for (int i = 0; i < 10; i++)
			System.out.println("Main thread"); // inner class
	}
}

class Test2 {

	public static void main(String[] args) {
		Thread t = new Thread(() -> {
			for (int i = 0; i < 10; i++)
				{System.out.println("Child Thread");
			System.out.println("Thread name "+Thread.currentThread().getName());}
			
		}); // lambda expression
		t.start();
		for (int i = 0; i < 10; i++) {
			System.out.println("Main Thread");
			System.out.println("Thread name "+t.getClass().getName());
		}
	}

}
