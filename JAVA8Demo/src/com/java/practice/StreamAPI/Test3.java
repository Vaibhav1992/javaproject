

package com.java.practice.StreamAPI;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Test3 {
 public static void main(String[] args) {
	 Date d1= new Date();
	 System.out.println("Todays date and time :"+d1);
	ArrayList<Integer> al1= new ArrayList<>();
	al1.add(1);
	al1.add(3);
	al1.add(8);
	al1.add(2);
	al1.add(76);
	System.out.println("before sorting Al"+al1);
	List<Integer> l1=al1.stream().map(i->i+5).collect(Collectors.toList());
	System.out.println("after sorting Al by map additon of  5 :"+l1);
	
	List<Integer> l2=al1.stream().filter(i->i%2==0).collect(Collectors.toList());
	System.out.println("after sorting Al by filter of  even no :"+l2);
	
	System.out.println("Total  no count in al greater than 5 is :"+al1.stream().filter(i->i>5).count());
	List<Integer> l3=al1.stream().sorted().collect(Collectors.toList());
	System.out.println("Sorting of Al asending'"+l3);
	
	List<Integer> l4=al1.stream().sorted((i1,i2)->i2.compareTo(i1)).collect(Collectors.toList());
	System.out.println("Sorting of Al desending'"+l4);
	System.out.println("Min value from AL is : "+al1.stream().min((i1,i2)->i1.compareTo(i2)).get());
	System.out.println("Max value from AL is : "+al1.stream().max((i1,i2)->i1.compareTo(i2)).get());
	
	
	al1.stream().forEach(i->System.out.println("Array Element  by for each method :"+i));
	
	
	
}
}
