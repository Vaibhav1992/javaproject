package com.java.practice.StreamAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class ALStreamAPI {

	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<>();
		al.add(1);
		al.add(4);
		al.add(2);
		al.add(11);
		al.add(0);
		al.add(99);
		System.out.println("Before sorting al"+al);
		List<Integer> l= al.stream().sorted().collect(Collectors.toList());
		List<Integer> l2= al.stream().sorted(((I1,I2)->(I1>I2)?-1:(I1<I2)?1:0)).collect(Collectors.toList());
		System.out.println("after natural sorting al"+l);
		System.out.println("after desending sorting al"+l2);
		ArrayList<String> s= new ArrayList<>();
		s.add("A");
		s.add("D");
		s.add("G");
		s.add("B");
		System.out.println("ArrayList before sorting bassed on alapahabetical orders is"+s);
		List<String> l3= s.stream().sorted().collect(Collectors.toList());
		List<String> l4= s.stream().sorted((v1,v2)->v2.compareTo(v1)).collect(Collectors.toList());
		System.out.println("ArrayList before sorting bassed on alapahabetical orders is"+l3);
		System.out.println("ArrayList before sorting bassed on alapahabetical  revers orders is"+l4);
		

	}
}
