package com.java.practice.StreamAPI;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.stream.Stream;

public class DateTime {
	
	public static void main(String[] args) {
		
		
		Double[] d={10.0,10.1,10.2,10.3};
		Stream s1=Stream.of(d);
		Stream s2=Stream.of(d);
		s1.forEach(System.out :: println);
		
		s2.forEach(s->System.out.println(s));
		LocalDate ld = LocalDate.now();
		LocalTime lt  =LocalTime.now();
		LocalDateTime ldDateTime = LocalDateTime.now();
		System.out.println("LocalDate"+ld);
		System.out.println("LocalDate"+lt);
		System.out.println("LocalDate"+ldDateTime);
		
	}

}
