package com.java.practice;
@FunctionalInterface
interface interf5{
	void methodone();
	default void method() {
		System.out.println("Default method of functional interface");
	}
	
	default void method2() {
		System.out.println("Default method of functional interface");
	}
	static void method3() {
		System.out.println("Static method of functional interface");
	}
}
public  class Default implements interf5 {

	public static void main(String[] args) {
		interf5 f5= new Default();
		f5.method();
		interf5.method3();
		
	}

	@Override
	public void methodone() {
		// TODO Auto-generated method stub
		
	}
}
