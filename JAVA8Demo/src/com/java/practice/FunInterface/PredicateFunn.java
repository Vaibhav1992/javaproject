package com.java.practice.FunInterface;

import java.util.function.Predicate;

public class PredicateFunn {

	public static void main(String[] args) {
		Predicate<Integer > p= (i)->(i==10);
		Predicate<String > p2= (s)->(s.length()>=3);
		
		System.out.println(p.test(10));
		System.out.println(p2.test("vaibhav1"));
	}
}
