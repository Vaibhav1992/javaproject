package com.java.practice.FunInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

	

public class lambadaCollection {
	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<Integer>();
al.add(10);
al.add(5);
al.add(15);
al.add(0);
al.add(100);

TreeSet<Integer> t = new TreeSet<Integer>();
TreeSet<Integer> t1 = new TreeSet<Integer>((I1,I2)->(I1>I2)?-1:(I1<I2)?1:0);
t.add(50);
t.add(0);
t.add(50);
t.add(48);
t.add(2);
t1.add(50);
t1.add(30);
t1.add(20);
t1.add(0);
t1.add(8);
t1.add(1);

System.out.println("Arraylist before sorting"+al);
Collections.sort(al,new MyComparator());
System.out.println("Arraylist after sorting by comparator class desending"+al);
Collections.sort(al,(I1,I2)->(I1>I2)?1:(I1<I2)?-1:0);

System.out.println("Arraylist after sorting by lambada expressionn class asending"+al);


System.out.println("Tresset by natural sorting"+t);
System.out.println("Tresset by lambda expression  sorting"+t1);


	}
	
}
