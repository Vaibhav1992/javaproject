package com.java.practice.FunInterface;

import java.util.ArrayList;
import java.util.Collections;



public class SortEmployee{
	public static void main(String[] args) {
		Employee e1= new Employee(1, "Vaibhav");
		Employee e2= new Employee(6, "vasantrao");
		Employee e3= new Employee(3, "Tushar");
		Employee e4= new Employee(11, "Tushar");
		Employee e5= new Employee(25, "Abcd");
		ArrayList<Employee> al =  new ArrayList<Employee>();
		al.add(e1);
		al.add(e2);
		al.add(e3);
		al.add(e4);
		al.add(e5);
	System.out.println("before sorting the employees"+al);
	Collections.sort(al,(v1,v2)->(v1.geteId() < v2.geteId())?-1:(v1.geteId() < v2.geteId())?1:0);
		
	System.out.println("after sorting the employees"+al);
	Collections.sort(al,(v1,v2)->(v1.geteName().compareTo((v2.geteName()))));
	System.out.println("after sorting the employees based on names "+al);
	
	}
}
