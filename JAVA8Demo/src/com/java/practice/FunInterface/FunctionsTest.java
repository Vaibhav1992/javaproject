package com.java.practice.FunInterface;

import java.util.function.Function;

public class FunctionsTest {

	public static void main(String[] args) {
		Function<String, Integer> f= (s)->s.length();
		System.out.println("length of Vaibhav :" +f.apply("Vaibhav"));
		System.out.println("length of Ram :" +f.apply("Ram"));
	}
}
